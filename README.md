# **NOTE** SCexport has been superseded by SCaddins #

# Introduction #

SCexport is a revit add-in I created to speed up and standardize file exports from whithin a Revit model.

SCexport is my first attempt at creating a Revit add-in, and my first program written in c#.
Hopefully someone will find this useful (and well enough written to be useful).


### SCexport can export the following files ###

* PDF (using Adobe Acrobat, if installed)
* DWG
* DGN
* DWF

### Other useful functions are: ###

* Assign revisions to multiple sheets.
* Quickly open and move back/forward through sheets.
* Print (very basic at the moment).


# Requirements #

* Revit 2012-2014
* Adobe Acrobat (optional for pdf export)
* Ghostscipt (optional for alternate pdf export)


# Installation #

* Close Revit
* Install the .msi package
* Start Revit, and run "SCexport" from the Add-ins tab.


# Configuration #

#### PDF export  configuration ####

If you have Adobe Acrobat installed, set the printer name (default "Adobe PDF")
in the options dialog, and it *should* work".
I only have access to Adobe Acrobat 9, so please let me know how other version go...

To use Ghostscript you will need to install a postcript printer. 
Ghostscript PDF printing is known to work well with the following printers:

* "HP Designjet T1100ps 44in PS3"

*Note*: the above printer works well because it contains a good selection of sheet sizes (A4-A0 at least).

#### Model Specific Configuration ####

* project configuration is done via an xml file:
	- this file can be created and accessed via the right click context 
	  menu within SCexport
	- it is named <revitfilename>.xml and resides in the same folder as the
	  Revit file.  If a central file exists it will be named as per the central
	  file (not the local user copy)
	  

Here's an example setting up a custom filename scheme:

```
#!XML

  <!--Standard scheme-->
  <FilenameScheme label="Architectural">
    <ProjectNumber/>
    <Hyphen/>
    <Discipline>AD</Discipline>
    <Hyphen/>
    <SheetNumber/>
    <Text>[</Text>
    <Revision/>
    <Text>]</Text>
  </FilenameScheme
```

# Compiling #
 
Scexport should compile for Revit 2012, 2013 and 2014. I have been using sharpdevelop 4.3, and linking revit dll's from the default install location.
The inculded msi installer is only for x64. 

	  

