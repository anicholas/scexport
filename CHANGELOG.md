##v3.2.0##

* Revit 2015 Support.
* Sheet / Sheet Number rename tool added.
* Much better per-user option settings & saving.
* Better build scripts.
* Default sheet naming changed.

##v3.1.0##

* Better sheet recognition in SCexport.OpenSheet

##v3.0.0##

* LGPL release

##v2.11.0##

* Experimental A3 print option added.
* Printer settings added to registry.
* Add revision option added

##v2.10.0##

* Search icon added.
* Allow open sheet shortcut to run from views aswell as sheets.

##v2.9.0##

* Union Square working folder shortcut added
* Press escape key to quit or to exit seatch feature.
* Better sheet size recognition (shouldn't crash if sheet size is incorrect).
* Open Next and Open Previous commands added.
* Open Sheet shortcut dialog added.

##v2.8.0##

* Beta printing option, disabeld until further testing.
* Tip of the day added (press 'T')
* Basic progress info added to progress bar

##v2.7.1##

*fixed bug in when selecting the file export type.

##v2.7.0##

*Advaced regex search function added to search both sheet number and sheet title.
*Better keybaord shortcuts help dialog.
*Automitic focus on main data grid.

##v2.6.0##

*Filter by latest revision ('L')

##v2.5.0##

*Scroll main view to currently open sheet ('S')

##v2.4.2##

*Format of scale column revised / cleaned-up.

##v2.4.2

*Make file overwrite confirmation always on top so it's not lost

##v2.4.1##

*Scale added to main view.
*Overwrite dialog added
*DWF export moded enables




