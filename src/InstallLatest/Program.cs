﻿/*
 * Created by SharpDevelop.
 * User: andrewn
 * Date: 15/05/14
 * Time: 1:35 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.IO;


namespace InstallLatest
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            var directory = new DirectoryInfo(args[0]);
            var myFile = directory.GetFiles("*.msi")
             .OrderByDescending(f => f.LastWriteTime)
             .First();
            
            Console.WriteLine(@"Installing: " + args[0] + @"\" + myFile);
            
            System.Diagnostics.Process.Start("msiexec.exe", @"/i " + args[0] + @"\" + myFile); 
            
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}