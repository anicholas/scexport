// (C) Copyright 2012-2014 by Andrew Nicholas (andrewnicholas@iinet.net.au)
//
// This file is part of SCexport.
//
// SCexport is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCexport is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCexport.  If not, see <http://www.gnu.org/licenses/>.

namespace SCexport
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using Autodesk.Revit.UI;
    using Autodesk.Revit.UI.Events;

    public partial class MainForm : Form
    {
        private SCexport scx;
        private Autodesk.Revit.DB.Document doc;
        private Autodesk.Revit.UI.UIDocument udoc;
        private FilterContextMenu filter;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        /// <param name="udoc"> The active revit uidoc.</param>
        public MainForm(Autodesk.Revit.UI.UIDocument udoc)
        {
            this.udoc = udoc;
            this.doc = udoc.Document;
            this.scx = new SCexport(this.doc);
            this.filter = new FilterContextMenu("Filter", -1, null);
            this.InitializeComponent();
            this.SetTitle();
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.PopulateViewSheetSetCombo();
            this.PopulateColumns();
            this.PopulateList(new ViewSheetSetCombo("<All views>")); 
            this.dataGridView1.Sort(this.dataGridView1.Columns[1], ListSortDirection.Ascending);
            this.dataGridView1.MultiSelect = true;
            this.KeyPreview = true;
            this.UpdateExportButton(0);
            this.dataGridView1.Focus();
            this.dataGridView1.Select();
        }
        
        /// <summary>
        /// Attempt to open the selected view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OpenSelectedViewToolStripMenuItem_Click(
                object sender, EventArgs e)
        {
            var dh = new DialogHandler(
                new UIApplication(this.udoc.Application.Application));
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows) {
                 var sc = row.DataBoundItem as SCexportSheet;
                if (sc == null) {
                    return;
                }
                 Autodesk.Revit.DB.FamilyInstance result =
                     SCexport.GetTitleBlockFamily(sc.SheetNumber, this.doc);
                if (result != null) {
                    this.udoc.ShowElements(result);
                }
            }

            this.Close();
        }

        private void SetTitle()
        {
            string version = SCexport.SCexportVersion.ToString();
            string name = 
                Assembly.GetExecutingAssembly().GetName().Name.ToString();
            this.Text = name + " [" + version + "]" + " by Andrew Nicholas";
        }

        private void PopulateViewSheetSetCombo()
        {
            var allViews = new ViewSheetSetCombo("<All views>");
            this.cmbPrintSet.Items.Add(allViews);
            foreach (ViewSheetSetCombo s in this.scx.AllViewSheetSets) {
                this.cmbPrintSet.Items.Add(s);
            }
        }

        private void PopulateList(FilterContextMenu f)
        {
            switch (f.Column) {
                case 1:
                    var bs1 = new SortableBindingList<SCexportSheet>(
                                  this.scx.AllSheets.Where(
                                      obj => obj.SheetNumber.StartsWith(f.Filter, StringComparison.CurrentCulture) == true).ToList());
                    this.dataGridView1.DataSource = bs1;
                    break;
                case 3:
                    var bs3 = new SortableBindingList<SCexportSheet>(
                                  this.scx.AllSheets.Where(
                                      obj => obj.SheetRevision.StartsWith(f.Filter, StringComparison.CurrentCulture) == true).ToList());
                    this.dataGridView1.DataSource = bs3;
                    break;
                case 5:
                    var bs5 = new SortableBindingList<SCexportSheet>(
                                  this.scx.AllSheets.Where(
                                      obj => obj.SheetRevisionDate.StartsWith(f.Filter, StringComparison.CurrentCulture) == true).ToList());
                    this.dataGridView1.DataSource = bs5;
                    break;
            }
        }

        private void PopulateList(string s)
        {
            var bs = new SortableBindingList<SCexportSheet>(
                    this.scx.AllSheets.Where(obj => obj.SheetNumber
                        .StartsWith(s, StringComparison.CurrentCulture) || 
                        obj.SheetNumber.StartsWith("DA" + s, StringComparison.CurrentCulture) == true).ToList());
            this.dataGridView1.DataSource = bs;
        }

        private void PopulateList()
        {
            this.dataGridView1.DataSource = this.scx.AllSheets;
        }

        private void PopulateList(ViewSheetSetCombo vss)
        {
            if (vss.CustomName == "<All views>") {
                this.PopulateList();
            } else {
                var bs = new SortableBindingList<SCexportSheet>();
                for (int i = 0; i < this.scx.AllSheets.Count; i++) {
                    foreach (Autodesk.Revit.DB.ViewSheet vs in vss.ViewSheetSet.Views) {
                        if (this.scx.AllSheets[i].Id == vs.Id) {
                            bs.Add(this.scx.AllSheets[i]);
                            break;
                        }
                    }
                }
                this.dataGridView1.DataSource = bs;
            }
        }

        private void AddColumn(string name, string text)
        {
            var result = new DataGridViewTextBoxColumn();
            result.DataPropertyName = name;
            result.HeaderText = text;
            this.dataGridView1.Columns.Add(result);
        }

        private void AddDateColumn(string name, string text)
        {
            var result = new DataGridViewTextBoxColumn();
            result.DataPropertyName = name;
            var style = new DataGridViewCellStyle();
            style.Format = "dd/MM/yyyy";
            result.DefaultCellStyle = style;
            result.HeaderText = text;
            this.dataGridView1.Columns.Add(result);
        }

        private void PopulateColumns()
        {
            this.dataGridView1.AutoGenerateColumns = false;
            this.AddColumn("FullExportName", "Export Name");
            this.AddColumn("SheetNumber", "Number");
            this.AddColumn("SheetDescription", "Name");
            this.AddColumn("SheetRevision", "Revision");
            this.AddColumn("SheetRevisionDescription", "Revision Description");
            this.AddDateColumn("SheetRevisionDateTime", "Revision Date");
            this.AddColumn("ExportDir", "Export Dir");
            this.AddColumn("Scale", "Scale");
            this.AddColumn("PageSize", "Page Size");
            this.AddColumn("PrintSettingName", "Print Setting");
        }

        private List<SCexportSheet> SelectedSheets()
        {
            var result = new List<SCexportSheet>();
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows) {
                var sc = row.DataBoundItem as SCexportSheet;
                result.Add(sc);
            }
            return result;
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            this.btnExport.Hide();
            this.btnPrint.Hide();
            this.btnFind.Hide();
            this.Refresh();
            this.SetUpPBar(this.NumberOfSelectedViews());
            this.scx.Export(
                this.SelectedSheets(),
                this.progressBar,
                this.progressInfo,
                this.statusStrip1);
            this.Close();  // end SCexport
        }

        private void SetUpPBar(int count)
        {
            this.statusStrip1.Show();
            this.progressBar.Minimum = 0;
            this.progressBar.Maximum = count;
            this.progressBar.Step = 1;
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                var s = (ViewSheetSetCombo)this.cmbPrintSet.SelectedItem;
                this.PopulateList(s);
            } catch {
            }
        }

        private void SelectAllOrNone(bool all)
        {
            if (all) {
                this.dataGridView1.SelectAll();
            }
            if (!all) {
                this.dataGridView1.ClearSelection();
            }
            this.UpdateExportButton(this.NumberOfSelectedViews());
            this.dataGridView1.Refresh();
        }

        private void MnuSelectAll_Click(object sender, EventArgs e)
        {
            this.SelectAllOrNone(true);
        }

        private void MnuItemExportDir_Click(object sender, EventArgs e)
        {
            DialogResult result = this.folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK) {
                this.folderBrowserDialog1.SelectedPath.ToString();
                this.scx.ExportDir = this.folderBrowserDialog1.SelectedPath;
            }
            this.dataGridView1.Refresh();
        }

        private void MnuVerify_Click(object sender, EventArgs e)
        {
            this.scx.Update();
            this.dataGridView1.Refresh();
        }

        private void MnuSelectNone_Click(object sender, EventArgs e)
        {
            this.SelectAllOrNone(false);
        }

        private void ShowHelp()
        {
            string s =
                "A\t    Select all" + System.Environment.NewLine +
                "C\t    Clear current filter" + System.Environment.NewLine +
                "D\t    Use _DELIVERABLES  as export folder" + System.Environment.NewLine +
                "J\t    Move selected row down" + System.Environment.NewLine +
                "K\t    Move selected row up" + System.Environment.NewLine +
                "L\t    Select *Latest* revision only" + System.Environment.NewLine +
                "N\t    Select none" + System.Environment.NewLine +
                "O\t    Open Selected Sheets" + System.Environment.NewLine +
                "P\t    Preliminary issue(date revision)" + System.Environment.NewLine +
                "Q\t    Quit" + System.Environment.NewLine +
                "S\t    Select current sheet only" + System.Environment.NewLine +
                "T\t    Tip of the day" + System.Environment.NewLine +
                "V\t    Verify selected sheets" + System.Environment.NewLine +
                "X\t    Start Export" + System.Environment.NewLine + System.Environment.NewLine +
                "0-9\t  Filter main view by sheet number" + System.Environment.NewLine + System.Environment.NewLine +
                "?\t    Help, show keyboard shortcuts (this dialog)" + System.Environment.NewLine +
                "/\t    Advanced search" + System.Environment.NewLine;
            var td = new TaskDialog("Keyboard Shortcuts");
            td.MainInstruction = "Keyboard Shortcuts";
            td.MainContent = s;
            td.Show();
        }

        private void MoveUpOrDown(int move)
        {
            int column = this.dataGridView1.CurrentCell.ColumnIndex;
            int row = this.dataGridView1.CurrentCell.RowIndex;
            if ((row + move != this.dataGridView1.Rows.Count) && (row + move >= 0)) {
                this.dataGridView1.CurrentCell = this.dataGridView1[column, row + move];
            }
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!this.searchBox.Visible) {
                switch (e.KeyChar.ToString().ToUpper()) {
                case "A":
                    this.SelectAllOrNone(true);
                    break;
                case "C":
                    this.PopulateList();
                    break;
                case "D":
                    this.MnuItemDeliverables_Click(sender, e);
                    break;
                case "J":
                    this.MoveUpOrDown(1);
                    break;
                 case "K":
                    this.MoveUpOrDown(-1);
                    break;
                case ":":
                    this.ShowOptions();
                    break;
                case "?":
                    this.ShowHelp();
                    break;
                case "/":
                    this.searchBox.Clear();
                    this.searchBox.Show();
                    this.searchBox.Focus();
                    break;
                case "L":
                    string s = SCexport.LatestRevisionDate();
                    if (s != null) {
                        this.PopulateList(new FilterContextMenu("temp", 5, s));
                    }
                    break;
                case "N":
                    this.SelectAllOrNone(false);
                    break;
                case "V":
                    this.MnuVerify_Click(sender, e);
                    break;
                case "O":
                    this.OpenSelectedViewToolStripMenuItem_Click(sender, e);
                    break;
                case "P":
                    this.scx.ForceDate = !this.scx.ForceDate;
                        this.dataGridView1.Refresh();
                    break;
                case "Q":
                    this.Close();
                    break;
                case "S":
                    this.SelectItem(SCexport.CurrentView());
                    this.dataGridView1.Refresh();
                    break;
                case "T":
                    TaskDialog.Show("Tip Of The Day", TipOfDay.Tip());
                    break;
                case "X":
                    this.BtnExport_Click(sender, e);
                    break;
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    this.PopulateList(e.KeyChar.ToString());
                    break;
                }
            }
        }

        /// <summary>
        /// Select multiple items from the main dataGrid.
        /// </summary>
        /// <param name="s">A string of the Sheet Number.</param>
        private void FilterItems(string s)
        {
            var bs = new SortableBindingList<SCexportSheet>();
            foreach (DataGridViewRow row in this.dataGridView1.Rows) {
                var sc = row.DataBoundItem as SCexportSheet;
                if (sc == null) {
                    return;
                }
                bool r1 = Regex.IsMatch(
                    sc.SheetNumber, s, RegexOptions.IgnoreCase);
                bool r2 = Regex.IsMatch(
                    sc.SheetDescription, s, RegexOptions.IgnoreCase);
                if (r1 || r2) {
                    bs.Add(sc);
                }
            }
            if (bs.Count > 0) {
                this.dataGridView1.DataSource = bs;
            }
        }

        /// <summary>
        /// Select one item from the main dataGrid.
        /// </summary>
        /// <param name="s">A string of the Sheet Number.</param>
        private void SelectItem(string s)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows) {
                var sc = row.DataBoundItem as SCexportSheet;
                if (sc.SheetNumber.Equals(s)) {
                    this.dataGridView1.ClearSelection();
                    row.Selected = true;
                    this.dataGridView1.FirstDisplayedScrollingRowIndex = row.Index;
                    this.dataGridView1.CurrentCell = this.dataGridView1[1, row.Index];
                    continue;
                }
            }
            this.UpdateExportButton(this.NumberOfSelectedViews());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.dataGridView1.Focus();
        }

        private void ToggleConversionFlag(
                ToolStripMenuItem box, SCexport.ExportFlags val)
        {
            if (box.Checked == true) {
                this.scx.AddExportFlag(val);
            } else {
                this.scx.RemoveExportFlag(val);
            }
        }

        private void ToggleCheckBoxValue(object sender, EventArgs e)
        {
            var c = (ToolStripMenuItem)sender;
            var t = (SCexport.ExportFlags)c.Tag;
            this.ToggleConversionFlag(c, t);
            this.UpdateExportButton(this.NumberOfSelectedViews());
        }

        private void UpdateExportButton(int count)
        {
            string s = "Export[" + count + "]:";
            if (this.scx.HasFlag(SCexport.ExportFlags.PDF)) {
                s += @" " + SCexport.ExportFlags.PDF.ToString();
            }
            if (this.scx.HasFlag(SCexport.ExportFlags.DWG)) {
                s += @" " + SCexport.ExportFlags.DWG.ToString();
            }
            if (this.scx.HasFlag(SCexport.ExportFlags.DWF)) {
                s += @" " + SCexport.ExportFlags.DWF.ToString();
            }
            if (this.scx.HasFlag(SCexport.ExportFlags.DGN)) {
                s += @" " + SCexport.ExportFlags.DGN.ToString();
            }
            if (this.scx.HasFlag(SCexport.ExportFlags.GS_PDF)) {
                s += @" " + SCexport.ExportFlags.GS_PDF.ToString();
            }
            this.btnExport.Text = s;
            this.dataGridView1.Refresh();
        }

        private void MnuSchemeSelected_Click(object sender, EventArgs e)
        {
            this.scx.SetFilenameScheme(sender.ToString());
            this.dataGridView1.Refresh();
        }

        private void ChangeLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.SourceLink);
        }

        private void MnuItemDeliverables_Click(object sender, EventArgs e)
        {
            this.scx.ExportDir = FileUtils.GetDeliverablesFolder(this.doc);
            this.dataGridView1.Refresh();
        }

        private void ToggleAutoCADVersion(object sender, EventArgs e)
        {
            var c = (ToolStripMenuItem)sender;
            this.scx.AcadVersion = (Autodesk.Revit.DB.ACADVersion)c.Tag;
        }

        private void CreateConfigFileToolStripMenuItem_Click(
                object sender, EventArgs e)
        {
            FileUtils.CreateConfigFile(ref this.doc);
        }

        private void EditConfigFileToolStripMenuItem_Click(
                object sender, EventArgs e)
        {
            FileUtils.EditConfigFile(ref this.doc);
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ab = new AboutBox1();
            ab.ShowDialog();
        }

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowHelp();
        }

      private void DataGridView1_CellMouseDown(
            object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1) {
                string s = "Filter";
                string f = "NA";
                this.filterToolStripMenuItem.Enabled = true;
                var row = (DataGridViewRow)this.dataGridView1.Rows[e.RowIndex];
                var sc = row.DataBoundItem as SCexportSheet;
                switch (e.ColumnIndex) {
                case 1:
                    char[] c =
                        new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                        f = "Filter: Rev *"; 
                    int index = sc.SheetNumber.IndexOfAny(c);
                    if (index > -1) {
                        f = "Filter: Starts with *"; 
                        this.filter.Update(
                                f + sc.SheetNumber.Substring(0, index + 1),
                                1,
                                sc.SheetNumber.Substring(0, index + 1));
                        s = this.filter.Label;
                    }
                    break;
                case 3:
                        f = "Filter: Rev *"; 
                        this.filter.Update(
                            f + sc.SheetRevision, 3, sc.SheetRevision);
                        s = this.filter.Label;
                        break;
                case 5:
                        f = "Filter: Rev Date*"; 
                        this.filter.Update(
                            f + sc.SheetRevisionDate, 5, sc.SheetRevisionDate);
                        s = this.filter.Label;
                        break;
                default:
                        this.filterToolStripMenuItem.Enabled = false; 
                        break;
                }
                filterToolStripMenuItem.Text = s;
            }
        }

        private void FilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.PopulateList(this.filter);
        }

        private void NoFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.PopulateList();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.ForumToolStripMenuItem_Click(sender, e);
        }

        private void ForumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.HelpLink);
        }

        private void DataGridView1_CellContentClick(
                object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6) {
                this.MnuItemExportDir_Click(sender, e);
            }
            if (e.ColumnIndex == 7) {
                this.MnuVerify_Click(sender, e);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private int NumberOfSelectedViews()
        {
            return this.dataGridView1.SelectedRows.Count;
        }

        private void DataGridView1CellMouseUp(
                object sender, DataGridViewCellMouseEventArgs e)
        {
                this.UpdateExportButton(this.NumberOfSelectedViews());
        }

        private void DataGridView1CellEndEdit(
                object sender, DataGridViewCellEventArgs e)
        {
            this.BindingContext[this.dataGridView1.DataSource].EndCurrentEdit();
        }

        private void DataGridView1MouseUp(object sender, MouseEventArgs e)
        {
            this.UpdateExportButton(this.NumberOfSelectedViews()); 
        }

        private void SearchBoxKeyDown(object sender, KeyEventArgs e)
        {
           if (e.KeyCode == Keys.Enter) {
                this.FilterItems(this.searchBox.Text);
                this.searchBox.Visible = false;
           }
        }

        private void Button1Click(object sender, EventArgs e)
        {
            this.scx.PrintA3(this.SelectedSheets(), this.scx.PrinterNameA3);
        }

        private void BtnExportResize(object sender, EventArgs e)
        {
            this.btnPrint.Location = new Point(
                this.btnExport.Location.X - (this.btnPrint.Width + 2),
                this.btnExport.Location.Y);
        }

        private void MainFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && !this.searchBox.Visible) {
                this.Close();
            } else if (e.KeyCode == Keys.Escape && this.searchBox.Visible) {
                this.searchBox.Visible = false;
            }
        }
        
        private void Button2Click(object sender, EventArgs e)
        {
            this.ShowOptions();
        }
        
        private void MnuExportOptionsClick(object sender, EventArgs e)
        {
            this.ShowOptions();
        }
        
        private void ShowOptions()
        {
            var options = new OptionsDialog(this.doc, this.scx, this.dataGridView1);
            options.ShowDialog();
            this.UpdateExportButton(this.NumberOfSelectedViews());
            this.dataGridView1.Refresh(); 
        }
        
        private void BtnFindClick(object sender, EventArgs e)
        {
            this.searchBox.Clear();
            this.searchBox.Show();
            this.searchBox.Focus();
        }
        
        private void AddRevisionToolStripMenuItemClick(object sender, EventArgs e)
        {
            #if REVIT2014
            SCexport.AddRevisions(this.SelectedSheets());
            this.Update();
            this.dataGridView1.Refresh();
            #else
            var td = new TaskDialog("Add Revision");
            td.MainIcon = TaskDialogIcon.TaskDialogIconWarning;
            td.MainContent = "Add revision only works with Revit 2014...time to upgrade hey.";
            td.Show();
            #endif            
        }
        
        private void RenameSelectedSheetsToolStripMenuItemClick(object sender, EventArgs e)
        {
            SCexport.RenameSheets(this.SelectedSheets());  
            this.Update();
            this.dataGridView1.Refresh();
        }
    }
}

/* vim: set ts=4 sw=4 nu expandtab: */
