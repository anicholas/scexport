// (C) Copyright 2012 by Andrew Nicholas
//
// This file is part of SCexport.
//
// SCexport is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCexport is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCexport.  If not, see <http://www.gnu.org/licenses/>.

using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SCexport")]
[assembly: AssemblyDescription(
"SC Export Utility\r\n" +
"\r\n" +
"A Revit add-in for batch exporting PDF & DWG files with a pre-defined file naming scheme.\r\n")
]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Andrew Nicholas")]
[assembly: AssemblyProduct("SCexport.Properties")]
[assembly: AssemblyCopyright("Copyright � Andrew Nicholas 2012-2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cc6c8be3-bbf2-420c-bd07-04505ee2e920")]

// Version information for an assembly consists of the following four values
//      Major Version   (major re-vamps)
//      Minor Version   (features)
//      Revision        (bugs)
//      Build           (build)
[assembly: AssemblyVersion("3.2.1.0")]
